package uk.ac.ebi.literature.textminingapi;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@PropertySources({ @PropertySource("classpath:application-utility.properties"),
		@PropertySource("classpath:application-utility-${spring.profiles.active}.properties"), })
@SpringBootApplication
@EnableMongoAuditing
public class TextminingApiOutcomeManagerApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(TextminingApiOutcomeManagerApplication.class);
		app.run(args);
	}

	public void run(String... args) throws Exception {

	}

}
