package uk.ac.ebi.literature.textminingapi;

import java.util.Arrays;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import uk.ac.ebi.literature.textminingapi.pojo.Components;
import uk.ac.ebi.literature.textminingapi.pojo.Failure;
import uk.ac.ebi.literature.textminingapi.pojo.FileInfo;
import uk.ac.ebi.literature.textminingapi.pojo.MLTextObject;
import uk.ac.ebi.literature.textminingapi.pojo.Status;
import uk.ac.ebi.literature.textminingapi.pojo.SubmissionMessage;
import uk.ac.ebi.literature.textminingapi.service.MLQueueSenderService;
import uk.ac.ebi.literature.textminingapi.service.MongoService;
import uk.ac.ebi.literature.textminingapi.utility.Utility;

@Service
public class TextminingApiOutcomeManagerService {

	private static final Logger log = LoggerFactory.getLogger(TextminingApiOutcomeManagerService.class);

	@Value("${urlFetcher.hostname}")
	private String urlFetcherHostName;

	@Value("${mongo.transaction}")
	private boolean transactionMongo;

	@Value("${rabbitmq.tmExchange}")
	private String textminingExchange;

	@Value("${rabbitmq.callbackQueue}")
	private String textminingCallbackQueue;

	private final MLQueueSenderService mlQueueSenderService;

	private final MongoService mongoService;

	public TextminingApiOutcomeManagerService(MLQueueSenderService mlQueueSenderService, MongoService mongoService) {
		this.mlQueueSenderService = mlQueueSenderService;
		this.mongoService = mongoService;
	}

	@RabbitListener(autoStartup = "${rabbitmq.outcomeQueue.autoStartUp}", queues = "${rabbitmq.outcomeQueue}")
	public void listenForMessage(Message message) throws Exception {
		var mlTextObject = Utility.castMessage(message, MLTextObject.class);

		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} Message arrived with Status {"
				+ mlTextObject.getStatus() + "}");

		if (mlQueueSenderService.hasExceededRetryCount(message)) {
			log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} retry count exceeded");
			var failure = Failure.valueOf(mlTextObject.getFtId(), mlTextObject.getUser(), mlTextObject.getFilename(),
					Components.OUTCOMEMANAGER.getLabel(), mlTextObject);
			log.info("printing Failure Object: " + failure.toString());
			this.mongoService.storeFailure(failure);
			return;
		}

		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} Finding Submission");

		var submissionMessage = this.mongoService.findSubmission(mlTextObject.getFtId(), mlTextObject.getUser());

		String fileName = mlTextObject.getFilename();

		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} Looking for {" + fileName
				+ "} in SubmissionMessage");

		Optional<FileInfo> optionalFile = findSubmittedFileByName(submissionMessage, fileName);

		if (optionalFile.isEmpty()) {
			log.error("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} File {" + fileName
					+ "} Not Found");
			throw new RuntimeException("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} File {"
					+ fileName + "} not found in the SubmissingMessage");
		}

		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} Setting status for FileInfo");

		updateFileInfo(mlTextObject, optionalFile.get());

		long pendingFilesCount = findCountByStatus(submissionMessage, Status.PENDING.getLabel());

		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} " + pendingFilesCount
				+ " files are pending");

		if (pendingFilesCount == 0) {

			log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} All the files are processed");

			long failureCount = findCountByStatus(submissionMessage, Status.FAILED.getLabel());

			Status status = (failureCount == 0) ? Status.SUCCESS : Status.FAILED;

			log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} there are {" + failureCount
					+ "} failures!");

			submissionMessage.setStatus(status.getLabel());

			log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} Sending SubmissionMessage to "
					+ textminingCallbackQueue);

			boolean sending = this.mlQueueSenderService.sendMessageToQueue(textminingCallbackQueue, submissionMessage,
					textminingExchange);

			if (sending == false) {
				throw new Exception("Impossible to store Success in Callback queue  for " + mlTextObject.toString());
			}

			log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} SubmissionMessage sent to "
					+ textminingCallbackQueue + " {" + sending + "}");
		}

		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} updating in SubmissionMessage");

		updateSubmissionMessage(submissionMessage);

		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} SubmissionMessage Updated!!!");
	}


	private final void updateSubmissionMessage(SubmissionMessage submissionMessage) throws Exception {
		if (this.transactionMongo)
			this.mongoService.updateSubmission(submissionMessage);
		else
			this.mongoService.updateSubmissionNoTransactional(submissionMessage);
	}


	private final void updateFileInfo(MLTextObject mlTextObject, FileInfo fileInfo) {
		if (mlTextObject.getStatus().equals(Status.FAILED.getLabel())) {
			fileInfo.setStatus(mlTextObject.getStatus());
			fileInfo.setErrorComponent(mlTextObject.getErrorComponent());
			fileInfo.setError(mlTextObject.getError());
		} else if (mlTextObject.getStatus().equals(Status.SUCCESS.getLabel())) {
			fileInfo.setStatus(Status.SUCCESS.getLabel());
			String urlFetcher = new StringBuilder(urlFetcherHostName).append(mlTextObject.getFtId()).append("/")
					.append(mlTextObject.getFilename()).toString();
			fileInfo.setUrlFetch(urlFetcher);
		}
	}


	private final Optional<FileInfo> findSubmittedFileByName(SubmissionMessage submissionMessage, String fileName) {
		return Arrays.stream(submissionMessage.getFiles()).filter(file -> file.getFilename().equals(fileName))
				.findFirst();
	}


	private final long findCountByStatus(SubmissionMessage submissionMessage, String status) {
		return Arrays.stream(submissionMessage.getFiles()).filter(file -> file.getStatus().equals(status)).count();
	}
}
